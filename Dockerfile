FROM openjdk
MAINTAINER hbc integration
ADD target/*.jar loyalty-points-service.jar
ENTRYPOINT ["java", "-jar", "/loyalty-points-service.jar"]
EXPOSE 2288