package com.hbc.integration.loyality.api.points;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import com.hbc.integration.loyality.api.authpoints.service.AuthPointsService;
import com.hbc.integration.loyality.api.cancelpoints.service.CancelPointsService;
import com.hbc.integration.loyality.common.util.MD5;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
//@EnableHystrixDashboarda
///@EnableCircuitBreaker
//@EnableDiscoveryClient
//@ComponentScan({"com.hbc.integration.loyality.api.authpoints"})
@EnableSwagger2
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public MD5 defaultSampler() {
		return new MD5();
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public AuthPointsService authPointsService() {
		return new AuthPointsService();
	}

	@Bean
	public com.hbc.integration.loyality.api.authpoints.service.CustomerShowService customerShowService() {
		return new com.hbc.integration.loyality.api.authpoints.service.CustomerShowService();
	}

	@Bean
	public CancelPointsService cancelPointsService() {
		return new CancelPointsService();
	}

	@Bean
	public com.hbc.integration.loyality.api.cancelpoints.service.CustomerShowService cancelPointCustomerShowService() {
		return new com.hbc.integration.loyality.api.cancelpoints.service.CustomerShowService();
	}

	@Bean
	public com.hbc.integration.loyality.api.estimatedearn.service.CustomerShowService estimateEarnShowService() {
		return new com.hbc.integration.loyality.api.estimatedearn.service.CustomerShowService();
	}

	@Bean
	public com.hbc.integration.loyality.api.estimatedearn.service.EstimatedEarnService estimateEarnService() {
		return new com.hbc.integration.loyality.api.estimatedearn.service.EstimatedEarnService();
	}

	@Bean
	public com.hbc.integration.loyalty.api.pointstransfer.service.PointsTransferService pointsTransferService() {
		return new com.hbc.integration.loyalty.api.pointstransfer.service.PointsTransferService();
	}

	@Bean
	public com.hbc.integration.loyality.api.settlepoints.service.CustomerShowService settlePointsCustomerShowService() {
		return new com.hbc.integration.loyality.api.settlepoints.service.CustomerShowService();
	}

	@Bean
	public com.hbc.integration.loyality.api.settlepoints.service.SettlePointsService settlePointsService() {
		return new com.hbc.integration.loyality.api.settlepoints.service.SettlePointsService();
	}
	
	@Bean
	public com.hbc.integration.loyalty.api.partners_points_award.service.LoyaltyPartnerServiceImpl loyaltyPartenersPointsService(){
		return new com.hbc.integration.loyalty.api.partners_points_award.service.LoyaltyPartnerServiceImpl();
	}

}