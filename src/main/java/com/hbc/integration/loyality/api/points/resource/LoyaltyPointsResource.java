package com.hbc.integration.loyality.api.points.resource;

import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hbc.integration.loyality.api.authpoints.model.AuthPointsRequest;
import com.hbc.integration.loyality.api.authpoints.model.AuthPointsResponse;
import com.hbc.integration.loyality.api.authpoints.resource.AuthPointsResource;
import com.hbc.integration.loyality.api.cancelpoints.model.CancelPointsRequest;
import com.hbc.integration.loyality.api.cancelpoints.model.CancelPointsResponse;
import com.hbc.integration.loyality.api.cancelpoints.resource.CancelPointsResource;
import com.hbc.integration.loyality.api.estimatedearn.model.EstimatedEarnRequest;
import com.hbc.integration.loyality.api.estimatedearn.model.MerkleEstimatedEarnResponse;
import com.hbc.integration.loyality.api.estimatedearn.resource.EstimatedEarnResource;
import com.hbc.integration.loyality.api.settlepoints.model.SettlePointsRequest;
import com.hbc.integration.loyality.api.settlepoints.model.SettlePointsResponse;
import com.hbc.integration.loyality.api.settlepoints.resource.SettlePointsResource;
import com.hbc.integration.loyalty.api.partners_points_award.exception.LoyaltyPartnerException;
import com.hbc.integration.loyalty.api.partners_points_award.model.LoyaltyPartnerRequest;
import com.hbc.integration.loyalty.api.partners_points_award.model.LoyaltyPartnerResponse;
import com.hbc.integration.loyalty.api.pointstransfer.model.PointsTransferRequest;
import com.hbc.integration.loyalty.api.pointstransfer.model.PointsTransferResponse;
import com.hbc.integration.loyalty.api.pointstransfer.resource.PointsTransferResource;

import io.swagger.annotations.ApiOperation;

@RestController
public class LoyaltyPointsResource {
	protected Logger logger = Logger.getLogger(LoyaltyPointsResource.class.getName());

	private AuthPointsResource authPointsResource = new AuthPointsResource();
	private CancelPointsResource cancelPointsResource = new CancelPointsResource();
	private EstimatedEarnResource estimatedEarnResource = new EstimatedEarnResource();
	private PointsTransferResource pointsTransferResource = new PointsTransferResource();
	private SettlePointsResource settlePointsResource = new SettlePointsResource();
	private com.hbc.integration.loyalty.api.partners_points_award.resource.LoyaltyPartnerResourceImpl  loyaltyPartnerResource =
			new com.hbc.integration.loyalty.api.partners_points_award.resource.LoyaltyPartnerResourceImpl ();

	
	@ApiOperation(value = "loyalty-partner service")
	@PostMapping(path = "/points/partners-points-award", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public LoyaltyPartnerResponse doService(@Valid @RequestBody LoyaltyPartnerRequest partnersPointsAwardRequest) throws LoyaltyPartnerException {
		return loyaltyPartnerResource.doService(partnersPointsAwardRequest);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/points/settle-points")
	public SettlePointsResponse addSettlePoints(@Valid @RequestBody SettlePointsRequest settlePointsRequest) {
		return settlePointsResource.addSettlePoints(settlePointsRequest);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/point/points-transfer")
	public PointsTransferResponse transferPoints(@Valid @RequestBody PointsTransferRequest pointsTransferRequest) {
		return pointsTransferResource.transferPoints(pointsTransferRequest);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/points/estimated-earns")
	public MerkleEstimatedEarnResponse getEstimatedEarn(@Valid @RequestBody EstimatedEarnRequest estimatedEarnRequest) {
		return estimatedEarnResource.getEstimatedEarn(estimatedEarnRequest);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/points/auth-points")
	public AuthPointsResponse test(@Valid @RequestBody AuthPointsRequest authPointsRequest) {

		return authPointsResource.addAuthpoints(authPointsRequest);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/points/cancel-points")
	public CancelPointsResponse addSettlePoints(@Valid @RequestBody CancelPointsRequest cancelPointsRequest) {
		return cancelPointsResource.addSettlePoints(cancelPointsRequest);
	}

	public CancelPointsResource getCancelPointsResource() {
		return cancelPointsResource;
	}

	public void setCancelPointsResource(CancelPointsResource cancelPointsResource) {
		this.cancelPointsResource = cancelPointsResource;
	}

	public AuthPointsResource getAuthPointsResource() {
		return authPointsResource;
	}

	public void setAuthPointsResource(AuthPointsResource authPointsResource) {
		this.authPointsResource = authPointsResource;
	}

	public EstimatedEarnResource getEstimatedEarnResource() {
		return estimatedEarnResource;
	}

	public void setEstimatedEarnResource(EstimatedEarnResource estimatedEarnResource) {
		this.estimatedEarnResource = estimatedEarnResource;
	}

	public PointsTransferResource getPointsTransferResource() {
		return pointsTransferResource;
	}

	public void setPointsTransferResource(PointsTransferResource pointsTransferResource) {
		this.pointsTransferResource = pointsTransferResource;
	}

	public SettlePointsResource getSettlePointsResource() {
		return settlePointsResource;
	}

	public void setSettlePointsResource(SettlePointsResource settlePointsResource) {
		this.settlePointsResource = settlePointsResource;
	}
	
	

	public com.hbc.integration.loyalty.api.partners_points_award.resource.LoyaltyPartnerResourceImpl getLoyaltyPartnerResource() {
		return loyaltyPartnerResource;
	}

	public void setLoyaltyPartnerResource(
			com.hbc.integration.loyalty.api.partners_points_award.resource.LoyaltyPartnerResourceImpl loyaltyPartnerResource) {
		this.loyaltyPartnerResource = loyaltyPartnerResource;
	}

	@RequestMapping("/points/starter")
	public String test() {
		logger.info("starter.test()");
		return "Loyalty Point Service Starter is Working.....";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public String message() {

		return "Loyalty Point Service terminal is up and running .....";
	}

	// =====================TESTING the
	// URLS=========================================
	
	@RequestMapping(method = RequestMethod.GET, value = "/points/pointsLoyaltyPartner/starter")
	public String loyaltyPartnerResourceStarter() {
		return loyaltyPartnerResource.testTheService() + " : Request from LoyaltyPartnerResource initiated";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/points/pointsTransfer/starter")
	public String pointsTransferResourceStarter() {
		return pointsTransferResource.test() + " : Request from PointsTransferResource initiated";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/points/settlePoints/starter")
	public String settlePointsResourceStarter() {
		return settlePointsResource.test() + " : Request from SettlePointsResource initiated";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/points/estimatedPoints/starter")
	public String estimatedEarnPointResourceStarter() {
		return estimatedEarnResource.test() + " : Request from EstimatedEarnPointsResource initiated";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/points/cancelPoints/starter")
	public String cancelPointsResourceStarter() {
		return cancelPointsResource.test() + " : Request from CancelPointsResource initiated";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/points/authPoints/starter")
	public String authPointsResourceStarter() {
		return authPointsResource.test() + " : Request from AuthPointsResource initiated";
	}
	
	// =====================TESTING ENDS=========================================
}
